<?php

function auditool_integration_settings_form() {
  
  $form['access-keys'] = array(
    '#type' => 'fieldset',
    '#title' => t('Auditool API keys'),
    /*'#description' => t('To obtain API keys, <a href="@signup-url">sign up</a> or log in to your <a href="@site-manager-url">Site manager</a>, register this site, and copy the keys into the fields below.', array(
      '@signup-url' => 'https://auditool.linnovate.net/pricing',
      '@site-manager-url' => 'https://auditool.linnovate.net/site-manager',
    )),*/
    '#collapsible' => TRUE,
    // Only show key configuration fields if they are not configured or invalid.
    '#collapsed' => TRUE,
  );
  $form['access-keys']['auditool_public_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Public key'),
    '#default_value' => variable_get('auditool_public_key', ''),
    '#element_validate' => array('auditool_integration_settings_validate_key', ''),
    '#description' => t('Used to uniquely identify this site.'),
  );
  $form['access-keys']['auditool_private_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Private key'),
    '#default_value' => variable_get('auditool_private_key', ''),
    '#element_validate' => array('auditool_integration_settings_validate_key', ''),
    '#description' => t('Used for authentication. Similar to a password, the private key should not be shared with anyone.'),
  );

  $form['auditool_integration_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Auditool Server Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  
  $form['auditool_integration_settings']['auditool_integration_settings_protocol'] = array(
    '#type' => 'select',
    '#title' => t('Protocol'),
    '#options' => array(
      'http' => t('http'),
      'https' => t('https')
    ),
    '#default_value' => variable_get('auditool_integration_settings_protocol', 'https'),
  );

  $form['auditool_integration_settings']['auditool_integration_settings_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Host'),
    '#description' => t('Put here the domain, i.e.: auditool.linnovate.net'),
    '#default_value' => variable_get('auditool_integration_settings_host', 'auditool.linnovate.net'),
  );

  $form['auditool_integration_settings']['auditool_integration_settings_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#default_value' => variable_get('auditool_integration_settings_port', '3000')
  );

  return system_settings_form($form);
}

